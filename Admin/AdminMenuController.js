﻿ angular.module('myApp.controllers')
    .controller('AdminMenuController', ['$state', '$location', '$rootScope', AdminMenuController]);


 function AdminMenuController($state, $location, $rootScope) {
     var self = this;

     //self.admin_menu_items = [

     //                         { url: '/#/admin/ruter', name: 'Ruter', icon: 'widgets' },
     //                        // { url: '/#/register', name: 'Register' },
     //                         { url: '/#/admin/customer_types', name: 'Customer Types', icon: 'perm_data_setting' },
     //                         { url: '/#/admin/portal_mapping', name: 'Unittype Mapping', icon: 'layers' },
     //                         { url: '/#/admin/user', name: 'Users', icon: 'people' },
     //                         { url: '/#/admin/settings', name: 'Settings', icon: 'settings' },

     //]

     self.admin_menu_items = null

     // Highligting selected menu button
     self.getClass = function (url) {
         if ('/#' + $location.path().substr(0, url.length - 2) == url || ($location.path() == '/home') && url == '/') {
             return "md-raised md-warn  md-hue-2 icons_left"
         } else {
             return "md-raised md-warn md-hue-1 icons_left"
         }
     }

 }

 