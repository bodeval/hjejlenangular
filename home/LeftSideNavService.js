﻿angular.module('myApp.services')
  .factory('diverseService', ['$http','$q', DiverseService]) ;

function DiverseService($http, $q) {

    var api_path = cs.api_url
      var o = {
          menu: [],
          list: []
      };

      o.getDataListMenu = function () {
          return $q(function (resolve, reject) {
              $http.get(api_path + '/api/diverse/GetDataListMenu')
              .success(function (result) {
                  o.menu = result;
                  return resolve(result);
              });
          });
      }

      o.getDataList = function (id) {
          return $q(function (resolve, reject) {
              $http.get(api_path + '/api/diverse/GetDataList?id=' + id)
              .success(function (result) {
                  return resolve(result);
              });
          });
      }     

      return o;
  };