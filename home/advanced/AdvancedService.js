﻿
angular.module('myApp.services')
    .factory('advancedService', ['$http', '$q', 'mainService', '$filter', '$rootScope', '$document', '$timeout', '$translate', AdvancedService]);

function AdvancedService($http, $q, mainService, $filter, $rootScope, $document, $timeout, $translate) {


    var api_path = cs.api_url



    var o = {}

    o.get_departures_by_date = function (departure_date, routes) {        
        return $q(function (resolve, reject) {           
            $http.get(api_path + '/api/diverse/GetDeparturesByDate?departure_date=' + departure_date + '&portal_id=' + mainService.portalId + '&ruter=' + routes)
                .success(function (result) {
                
                    return resolve(result)
                });
        });       
    }

    return o;
};