﻿
angular.module('myApp.services')
.factory('ruterAdminService', ['$http', '$q', RuterAdminService]);

function RuterAdminService($http, $q) {

    var token_server_address = cs.api_url
    alert(token_server_address)
    var self = this;
    var o = {

    };


    o.get = function () {
        return $q(function (resolve, reject) {
            
            $http({
                method: 'GET',
                url: token_server_address + '/api/Routes/get'

            }).success(function (result) {
                
                return resolve(result);
            });
        });
    }

    o.get_portal_rutes = function () {
        return $q(function (resolve, reject) {

            $http({
                method: 'GET',
                url: token_server_address + '/api/Routes/GetPortalRoutes'

            }).success(function (result) {
                
                return resolve(result);
            });
        });
    }

    o.save_portal_rutes = function(routes) {
        return $q(function (resolve, reject) {

            $http({
                method: 'POST',
                url: token_server_address + '/api/Routes/SaveRoutes',
                data: { routes: routes }

            }).success(function (result) {

                return resolve(result);
            });
        });
    }
    

    return o;
}
