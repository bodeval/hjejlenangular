﻿(function () {

    angular
        .module('myApp.controllers')
        .controller('AdvancedRightController', [
            '$mdSidenav', '$mdBottomSheet', '$timeout', '$log', '$document', '$scope', '$mdToast', '$rootScope', 'mainService', 'tourService', '$window', '$filter', 'advancedService', 'authService', '$mdDialog', '$translate', '$mdSidenav',
            AdvancedRightController
        ]);



    function AdvancedRightController($mdSidenav, $mdBottomSheet, $timeout, $log, $document, $scope, $mdToast, $rootScope, mainService, tourService, $window, $filter, advancedService, authService, $mdDialog, $translate, $mdSidenav) {
        var primary_harbor_id = 23
        var primary_harbor_name = "Silkeborg"

        var secundary_harbor_id = 32
        var secundary_harbor_name = "Himmelbjerget"

        var self = this;
        self.user_info = null
        self.authentication = authService.authentication


        self.toggle_sidenav = function (elmId) {            
            $scope.toggleLeft()
        }
        
        function buildToggler(componentId) {
            return function () {
                $mdSidenav(componentId).toggle();
            };
        }
        $scope.toggleLeft = buildToggler('left');

        self.toggle_departure = null


        $scope.$window = $window;
        self.departures_is_selected = false
        self.selected_index = 0
        self.debug = true
        self.selection_step = 0;

        self.selected_departure = null
        self.selected_arrival = null
        self.departures = []

        self.selected_departures = []
        self.tours = [[]]

        self.time_tracker =null
        self.comment = ""


        self.get_departure_info = function (event, departure) {
            console.log(departure)

            event.stopPropagation();

            self.toggle_departure = departure            

            $mdSidenav('left')
                .open()
                .then(function () {
                    $log.debug('opened');
                });

        }

        self.hide_info = function () {
            $mdSidenav('left')
                .close()
        }

        self.clear_single = function (departure) {

            console.log(departure)
            console.log(angular.copy(self.tours))
          
            if (self.tours.length < 3) {
                self.reset()
                return false;
            }

           

            
            var last_dep

            var index
            $.each(self.tours, function (index2, elm) {
                if ($.grep(elm, function (x) {
                    return x.Afgangs_id == departure.Afgangs_id;
                }).length > 0) {

                    index = index2
                    
                    $.each(elm, function (index3, elm3) {
                        elm3.selected_departure_id = 0

                        elm3.selected_departure = false
                        elm3.selected_css = ""
                        elm3.selectable = false
                        
                        elm3.selected_departure_id = 0
                        elm3.selected_departure = false

                        if (elm3.selected_arrival == true) {
                            
                            self.check_for_pause(elm3)
                            self.on_arrival_selected(elm3)
                            var y = elm[index3 + 1]
                            if (y) {
                                y.selected_arrival = false
                                y.selected_css = ""
                                y.selectable = false
                                y.selected_departure = false
                                self.check_for_pause(elm3)
                                self.on_arrival_selected(y)
                                y.selected_departure_id = 0
                            }
                        }

                    })
                    last_dep = elm[0]
                    


                    return false
                   
                }
            })

            self.tours.splice(index, 1);

            var last_tour = self.tours[self.tours.length - 2]
            

            self.time_tracker = new Date(last_tour[last_tour.length - 1].DatoTid)
            self.on_arrival_selected(last_dep)
            
            


           
        }

        self.add_note = function (ev) {

            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.prompt()
                .title(t('Comment'))
                .textContent(t('to_this_booking'))
                .placeholder(t('comment_placeholder'))
                .ariaLabel(t('comment'))
                .initialValue(self.comment)
                .targetEvent(ev)
                //.required(true)
                .ok('Ok')
                .cancel(t('cancel'));

            $mdDialog.show(confirm).then(function (result) {


                self.comment = result
                mainService.comment = result

            }, function () {
                self.comment = ''
                mainService.comment = ''

            });
        }

        self.reset = function () {
            
            self.selected_departures = []
            
                self.tours = [[]]
            
            
            self.time_tracker = null
            self.get_departures()

            self.selection_step = 0
           
        }


       
        self.add_tour = function (step) {
            if (step == 2) {                
                self.selected_departure = null
                self.selected_arrival = null
                self.selection_step = 0                
            }            
        }

        self.click_departure = function (departure) {
            
            
            if (departure.selectable == false && !departure.clickable == true) {
                return false;
            }

            if (self.time_tracker) {
                if (new Date(departure.DatoTid) < self.time_tracker) {
                    return false;
                }
            }
          
            if (self.selection_step == 1) {
                if (new Date(departure.DatoTid) < self.time_tracker) {
                    return false;
                }

                if (self.check_for_pause(departure) == false) {                    
                    return false;
                }
            }

            if (self.selected_departure) {
                if (new Date(departure.DatoTid) < self.time_tracker) {
                    return false;
                 }
            }

          

            switch (self.selection_step){            
                case 0:

                    if (departure.is_disabled) {
                        return false;
                    }

                    if (departure.is_start_slut == true) {                        
                        break;
                    }
                    departure.selected_departure = true
                    departure.selected_css = "selected_departure "
                    departure.selectable = false
                    self.selected_departure = departure;
                    self.selection_step = 1

                    self.on_departure_selected(departure)

                    self.time_tracker = new Date(departure.DatoTid)
                    break;
                case 1:

                    
                    if (self.selected_departure.starthavn_id == primary_harbor_id && departure.sluthavn_id == primary_harbor_id) {
                      
                        break;

                    }

                    departure.selected_departure = false;
                    departure.selected_arrival = true;                    
                    self.selected_arrival = departure
                    if (self.selected_arrival.Afgangs_id != self.selected_departure.Afgangs_id) {                        
                        self.select_tour_departures(departure)
                    } else {
                        self.selected_departures.push(departure)
                        self.selected_departures.push(departure)
                    }
                    
                    self.tours[self.tours.length - 1] = self.selected_departures
                    self.tours.push([])

                    self.selected_departures = []
                    //self.selection_step = 2                                        
                    self.selected_departure = null
                    self.selected_arrival = true
                    self.selection_step = 0

                    
                    self.time_tracker = new Date(departure.DatoTid)
                    self.on_arrival_selected(departure)
                    break;
                case 2:
                    break            
            }      
        }

        self.select_tour_departures = function (departure) {
            var selected_departure_id;
            var selected_ferry_id;
            var naeste;
            var previous;
            $.each(self.departures, function (index, elm) {

                var afgangs_id = elm.Afgangs_id
                var ferry_id = elm.Fartoj_id

                if (afgangs_id == self.selected_departure.Afgangs_id) {
                    elm.selected_departure_id = afgangs_id
                    naeste = elm.naeste;
                    self.selected_departures.push(elm)
                }
        
                if (departure.Afgangs_id == previous) {
                    self.selected_departures.push(elm)
                    return false;
                }

                if (naeste == elm.Afgangs_id) {                    
                    naeste = elm.naeste;

                    previous = elm.Afgangs_id;

                    elm.selected_departure_id = afgangs_id
                    self.selected_departures.push(elm)
                    if (elm.selected_arrival == true) {
                        
                        return false;
                    }
                    
                }    
            })
        }

        self.on_arrival_selected = function (departure) {

            var selectable_departures = []
            var found = false;
            var naeste;
            
            $.each(self.departures, function (index, elm) {

              //  (d.DatoTid < self.time_tracker ? ' unselectable' : '')
                elm.selectable = false

                var afgangs_id = elm.Afgangs_id
                var ferry_id = elm.Fartoj_id

                if (afgangs_id == departure.Afgangs_id) {

                    naeste = elm.naeste;
                    found = true;
                } else if (found) {

                    if (new Date(elm.DatoTid) > self.time_tracker) {
                        selectable_departures.push(elm)
                    }
                    
                    //if (elm.sluthavn_id == primary_harbor_id || elm.sluthavn_id == secundary_harbor_id) {
                    //    //var last_elm = self.departures[index ]                        
                    //    //selectable_departures.push(last_elm)
                    //    return false;
                    //}
                    naeste = elm.naeste;
                }
                if (found) {
                    $.each(selectable_departures, function (index, elm) {
                        elm.selectable = true
                    })
                }
            })
        }

        self.on_departure_selected = function (departure) {
            
            var selectable_departures = []
            var found = false;
            var naeste;
            $.each(self.departures, function (index, elm) {
                elm.selectable = false
                var afgangs_id = elm.Afgangs_id
                var ferry_id = elm.Fartoj_id

                if (afgangs_id == self.selected_departure.Afgangs_id) {

                    naeste = elm.naeste;
                    found = true;
                } else if (naeste == afgangs_id) {

                    selectable_departures.push(elm)
                    if (elm.sluthavn_id == primary_harbor_id || elm.sluthavn_id == secundary_harbor_id) {
                        //var last_elm = self.departures[index ]                        
                        //selectable_departures.push(last_elm)
                        return false;
                    }
                    naeste = elm.naeste;
                }

            })

            if (found) {

                $.each(self.departures, function (index, elm) {
                    elm.selectable = false
                })

                $.each(selectable_departures, function (index, elm) {
                    elm.selectable = true
                })
            }
            
        }

        self.check_for_pause = function (departure) {
            var selected_departure_id;
            var selected_ferry_id;
            var naeste;
            var result;

            

            $.each(self.departures, function (index, elm) {

                var afgangs_id = elm.Afgangs_id
                var ferry_id = elm.Fartoj_id
                
                if (afgangs_id == self.selected_departure.Afgangs_id) {
                    naeste = elm.naeste;
                }

              
                if (elm.starthavn_id == secundary_harbor_id && elm.Afgangs_id == departure.Afgangs_id) {

                    if (departure.is_start_slut) {
                        result = true
                    } else {
                        result = false
                    }
                    
                    return false
                }

                if (naeste == elm.Afgangs_id) {
                    naeste = elm.naeste;
                    if (elm.Afgangs_id == departure.Afgangs_id) {

                        result = true;
                        return false;
                    } else if (elm.starthavn_id == secundary_harbor_id) {
                        
                        if (departure.is_start_slut) {
                            
                            result = true;
                        } else {
                            
                            result = false;
                        }

                        return false;
                    } else if (elm.starthavn_id == primary_harbor_id  && afgangs_id != self.selected_departure.Afgangs_id) {
                        
                        result = false
                        return false
                    } 
                }

            })  
            
            return result
        }

        self.check_start_slut = function(departure) {
            
            if (departure.is_start_slut) {

                var dep =  $.grep(self.departures, function (elm) {
                    return elm.Afgangs_id == departure.Afgangs_id
                })

                var result = dep[0].selectable
                if (result) {
                    departure.clickable = true
                }
                return result
            } else {
                return false
            }
            
        }

        self.get_selection_css = function (d) {
            var result
            
                result = d.selected_departure == true ? 'selected_departure' : d.selected_arrival == true ? 'selected_arrival' : d.selected_departure_id > 0 ? 'selected_crossing' : '';
              //  result += (d.DatoTid < self.time_tracker ? ' unselectable' : '')
           
            return result
        }

        /**
         * *************************Dates ****************************************
         */

        var today = new Date()
        self.minDate = new Date(
            today.getFullYear(),
            today.getMonth(),
            today.getDate());

        self.maxDate = json2date(cs.settings.maxDate)
        self.md_datepickerStartDate = today
        self.first_date = today
       

        /****************************** Help Functions ******************************/

        self.in_time = function (item) {
            var item_date = json2date(item.DatoTid)
            var lukketid = item.Akt_lukketid

            // hvis det er telefærge f.x. hjorø skal aktlukketid bruges
            if (item.Akt_lukketid_IfBookings) {
                if (item.SumBookings > 0) {
                    lukketid = item.Akt_lukketid_IfBookings
                }
            }
            var item_lukketid = $filter('minutesToDateTime')(item_date, -lukketid)
            return item_lukketid >= new Date()
        }

        self.check_is_disabled = function (item) {
            item.in_time = self.in_time(item)
            var result = (!item.in_time) || item.Udsolgt == true || item.Locked == true || mainService.number_of_persons + item.SumPersons > (authService.authentication.isAuth ? item.MaxPers : (item.MaxPers * item.PersonBookSelvPct / 100))  || ( item.SumNumberBikes + tourService.selected_unit_type.number_of_items > item.max_number_of_bikes ? true : false) 
            item.is_disabled = result
            return result
        }

        self.get_departures = function () {
            self.loading = true
            advancedService.get_departures_by_date(moment(self.first_date).format("MM-DD-YYYY"), '18')
                .then(function (result) {
                    var start_ture_silkeborg = []
                    
                    var start_ture_himmelbjerget = []
                    var parent_is_disabled
                    var parent_in_time
                    self.departures = result

                    $.each(result, function (index, elm) {
                        self.check_is_disabled(elm)
                        elm.selected_departure = false
                        elm.selectable = false
                        elm.selected_departure_id = 0

                        if (elm.starthavn_id == self.starthavn.id) {
                            if (!elm.is_disabled && elm.in_time) {
                                elm.selectable = true
                            } else {
                                elm.disabled = true
                            }                            
                        }                        

                        if (elm.starthavn_id == primary_harbor_id) {
                            elm.disabled_arrival = elm.is_disabled
                            elm.in_time_arrival = elm.in_time
                            start_ture_silkeborg.push(elm)
                        }

                        else if (elm.starthavn_id == secundary_harbor_id) {
                            elm.disabled_arrival = elm.is_disabled
                            elm.in_time_arrival = elm.in_time
                            start_ture_himmelbjerget.push(elm)
                        } else {
                            //elm.disabled_arrival = parent_is_disabled
                            //elm.in_time_arrival = parent_in_time
                            elm.in_time_arrival = elm.in_time
                                elm.disabled_arrival = false
                          
                        }

                        parent_is_disabled = elm.is_disabled
                        parent_in_time = elm.in_time
                    })


                    self.tours_silkeborg = []
                    self.tours_himmelbjerget = []
                    $.each(start_ture_silkeborg, function (index, elm) {

                        var tour = []

                        $.each(result, function (index2, elm2) {                            
                            
                            if (elm == elm2) {
                                tour = []
                                tour.push(elm2)
                            } else {
                                if (tour.length > 0) {
                                    if (elm2.Fartoj_id == elm.Fartoj_id) {

                                        tour.push(elm2)
                                        if (elm2.sluthavn_id == secundary_harbor_id) {

                                            var extra = angular.copy(elm2)
                                            extra.starthavn = secundary_harbor_name
                                            extra.DatoTid = $filter('minutesToDateTime')(result[index2].DatoTid, result[index2].Overfartstid)
                                            extra.is_start_slut = true
                                            
                                            tour.push(extra)

                                            self.tours_silkeborg.push(tour)
                                            return false;
                                        }
                                    }
                                }
                            }
                        })
                    })


                    $.each(start_ture_himmelbjerget, function (index, elm) {

                        var tour = []

                        $.each(result, function (index2, elm2) {                            
                            if (elm == elm2) {
                                tour = []
                                tour.push(elm2)
                            } else {
                                if (tour.length > 0) {
                                    if (elm2.Fartoj_id == elm.Fartoj_id) {

                                        tour.push(elm2)
                                        if (elm2.sluthavn_id == primary_harbor_id) {
                                            var extra = angular.copy(elm2)
                                            extra.starthavn = primary_harbor_name
                                            extra.DatoTid = $filter('minutesToDateTime')(result[index2].DatoTid, result[index2].Overfartstid)
                                            extra.is_start_slut = true
                                            tour.push(extra)

                                            self.tours_himmelbjerget.push(tour)
                                            return false;
                                        }
                                    }
                                }
                            }
                        })
                        
                    })

                  


                    var departures_silkeborg_view = []
                   
                    $.each(self.tours_silkeborg[0], function (index, elm) {
                        var row = []
                        row.push(elm.starthavn)
                        $.each(self.tours_silkeborg, function (index2, elm2) {
                            row.push(elm2[index])
                        })
                        departures_silkeborg_view.push(row)
                    })
                    
                    self.departures_silkeborg = departures_silkeborg_view

                    var departures_himmelbjerget = []

                    $.each(self.tours_himmelbjerget[0], function (index, elm) {
                        var row = []
                        row.push(elm.starthavn)
                        $.each(self.tours_himmelbjerget, function (index2, elm2) {
                            row.push(elm2[index])
                        })

                       departures_himmelbjerget.push(row)
                    })

                    self.departures_himmelbjerget = departures_himmelbjerget

                    self.loading = false
                })
        }

        var t = function (key) {
            return $translate.instant(key)
        }

        self.get_last_tour = function () {
            return tourService.get_last_tour()
        }

        // change language
        self.get_language = function (item) {
            try {
                if (item) {
                    return item.Languages[$rootScope.language.toUpperCase()]
                }
            }
            catch (e) {
                console.log(e)
            }
        }

        self.t = function (key) {
            return $translate.instant(key)
        }

       

        self.admin_book = function (ev) {
            var user_info = { name: '', email: '', phone: '' }

            $mdDialog.show({
                locals: { user_info: user_info },
                controller: ['$scope', '$mdDialog', '$mdToast', 'user_info', UserInfoController],
                controllerAs: 'ctrl',
                templateUrl: '/content/angular/home/_user_info.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                preserveScope: true,
                clickOutsideToClose: false,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            }).then(function (answer) {
                self.book(answer)

            }, function () {
                self.user_info = null
            });
        }



        self.book = function (user_info) {

            if (self.selection_step == 1 | self.tours.length == 1) {
                return false;
            }
            
            self.loading = true
            var book_unit_type = tourService.selected_unit_type

            mainService.advanced_book(book_unit_type, self.tours, user_info, self.departures[self.departures.length - 1])
                .then(function (result) {
                    if (result.Success) {

                        if (user_info != undefined) {

                            var txt = "Der bliver nu oprettet en booking "
                            var toast = $mdToast.simple()
                                .textContent(txt)
                                .position('bottom right')
                                .hideDelay(3000)
                                .highlightAction(true)
                                .parent("#adv_header");


                            $mdToast.show(toast).then(function () {
                                document.location.href = "/"
                            })

                        } else {
                            if (result.Uri) {
                                document.location.href = result.Uri
                            }
                        }

                    } else {
                        if (result.MessageList) {
                            self.show_toast(result.MessageList.join(","))
                        } else {
                            // if admin booking ok
                            //$location.path('/');
                            document.location.href = "/"
                        }

                        self.loading = false
                        self.tours[0].selected_departure = null
                        self.is_ready_to_book = false
                    }
                })
        }

        

        $scope.$on("done", function () {

            self.starthavn = mainService.home_harbor
            self.first_date = tourService.start_date
           // self.first_date = new Date().addDays(30)
            self.get_departures()
            self.unit_types = mainService.unit_types
            self.departure_harbors = mainService.harbors
        });        
    }
})();
