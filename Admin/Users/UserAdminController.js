﻿

    angular.module('myApp.controllers')
    .controller('UserAdminController', ['UserAdminService', '$scope', '$mdDialog', '$mdToast', '$rootScope', 'authService', '$location',  UserAdminController]);

    function UserAdminController(UserAdminService, $scope, $mdDialog, $mdToast, $rootScope, authService, $location) {
        var self = this;

        
        self.show_admin = false;
        self.roles = [];
        self.users = [];
        self.languages = [
            { name: 'Danish', code: 'da' },
            { name: 'English', code: 'en' },
            { name: 'German', code: 'de' },
            { name: 'Swedish', code: 'se' }
        ]
      

      

        self.new_user = function (ev) {
            


             $mdDialog.show({
                locals: { info: "" },               
                controller: ['$scope', '$mdDialog', '$mdToast', RegisterUserController],
                controllerAs: 'ctrl',
                templateUrl: '/content/angular/Admin/Users/_register_user.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                preserveScope: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
             .then(function (answer) {
                 $scope.status = 'You said the information was "' + answer + '".';
                 self.get_users();
                 self.show_toast(answer)
             }, function () {
                 $scope.status = 'You cancelled the dialog.';
             });
        }
      

        self.get_users = function () {
            UserAdminService.GetUsers()
            .then(function (result) {
                $.each(result, function (index, elm) {
                    if (elm.roles) {                        
                        elm.roles_array = elm.roles.split(",")
                    } else {
                        elm.roles_array = []
                    }                    
                })

                self.users = result;
               //$scope.user_form.$setPristine()
            })
        };

        self.get_roles = function () {
            UserAdminService.GetRoles()
            .then(function (result) {                
                self.roles = result;
                self.get_users()
            })
        }
        self.get_roles()
        self.role_id_checked = function (id, user) {
            return user.roles_array.indexOf(id) > -1
        }

        self.role_check = function (id, user) {
            var index = user.roles_array.indexOf(id);
            if (index > -1) {
                user.roles_array.splice(index, 1)
            } else {
                user.roles_array.push(id)
            }
            user.changed = true
        }

        self.save_user_roles = function (user) {
            user.roles = user.roles_array.join(",")
            UserAdminService.save_user_roles(user)
            .then(function (result) {
                UserAdminService.save_user(user)
                .then(function (result) {
                    user.changed = false;
                    self.show_toast("User are saved!")
                })
                
            })
        }

        self.show_toast = function (txt) {
            var toast = $mdToast.simple()
               .textContent(txt)
               .position('top right')
               .hideDelay(3000)
               .highlightAction(true)
               .parent("#toast_div");
            //.theme("custom-toast");                

            $mdToast.show(toast)
        }

        self.delete_user = function (ev,user) {
            var confirm = $mdDialog.confirm()
		          .title('Delete user:' + user.Email)
		          .textContent('')
		          .ariaLabel('Delete User')
		          .targetEvent(ev)
		          .ok('Ok!')
		          .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                var index = self.users.indexOf(user)
            
                UserAdminService.delete_user(user)
                .then(
                function (result) {

                    self.users.splice(index,1)

                    self.show_toast("User deleted!")
                },
                function (error) {
                    self.show_toast(error)
                })
            })
        }


        function RegisterUserController($scope, $mdDialog, $mdToast) {

            var self = this;

            self.title = "New User";

            self.email = ""
            self.password = ""
            self.show_password = true;


            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };


            

            self.generate_password = function (length) {

                var chars = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz!@#$%&()_<>ABCDEFGHIJKLMNOP1234567890ABCDEFGHIJKLMNOP123456789012345678901234567890";
                var pass = "";
                for (var x = 0; x < length; x++) {
                    var i = Math.floor(Math.random() * chars.length);
                    pass += chars.charAt(i);
                }
                return pass;
            }

            self.save_user = function () {
         
                 self.downloading = true;
                 UserAdminService.register(self.email, self.password)
                 .then(
                    function (result) {
                        self.register_error = "";
                        self.downloading = false;
                        self.login_result = 'New User: ' + self.email                        
                        $mdDialog.hide(self.login_result);
                        
                    },
                    function (message) {
                        self.downloading = false;
                        if (message.ModelState) {
                            self.register_error = message.ModelState[""]
                        }
                    }
                 )
            }

        }

    }



