﻿(function () {
    angular.module('myApp.services')
    .factory('UserAdminService', ['$http', '$q', UserAdminService]);

    function UserAdminService($http, $q) {
       
        var token_server_address = ""
        var self = this;
        var o = {
           
        };


        o.GetRoles = function () {
            return $q(function (resolve, reject) {
            
                $http({
                    method: 'GET',
                    url: token_server_address + '/api/Account/GetRoles',
                }).success(function (result) {
                    self.roles = result
                    return resolve(result);
                });
            });
        }

      
        o.GetUsers = function () {
            return $q(function (resolve, reject) {
                $http({
                    method: 'GET',
                    url: '/api/Account/GetUsers',                    
                }).success(function (result) {
                    return resolve(result);
                });

            });
        }


        o.save_user = function (user) {

            return $q(function (resolve, reject) {
                $http({
                    method: 'POST',
                    url: '/api/Account/SaveUser',
                    data: user
                }).success(function (result) {
                    return resolve(result);
                });

            });
        }

        o.save_user_roles = function (user) {
            
            return $q(function (resolve, reject) {
                $http({
                    method: 'POST',
                    url: '/api/Account/SaveUserRoles',
                    data: user
                }).success(function (result) {
                    return resolve(result);
                });

            });
        }


        o.delete_user = function (user) {            
            return $q(function (resolve, reject) {
                user.roles = '0'
                o.save_user_roles(user)
                      .then(function () {
                          $http({
                              method: 'GET',
                              url: '/api/Account/DeleteUser?Id=' + user.Id,                    
                          }).success(function (result) {
                              return resolve(result);
                          });
                      })
                });              
        }

         o.register = function (username, password) {
            return $q(function (resolve, reject) {               

                $http({
                    method: 'POST',
                    url: token_server_address + '/api/Account/Register',
                    
                    data: { Email: username, Password: password, ConfirmPassword: password }

                }).success(function (result) {
                    return resolve(result);
                }).error(function (message) {
                    reject(message)
                 })
            });
        }

        return o;
    }
})();