﻿
angular.module('myApp.services')
.factory('settingsAdminService', ['$http', '$q', SettingsAdminService]);

function SettingsAdminService($http, $q) {

    var token_server_address = cs.api_url
    var self = this;
    var o = {

    };

    
    o.get = function () {
        return $q(function (resolve, reject) {

            $http({
                method: 'GET',
                url: token_server_address + '/api/Settings/Get',                

            }).success(function (result) {              
                return resolve(result);
            });
        });
    }



    o.save = function (settings) {
        return $q(function (resolve, reject) {

            var user_settings = {}

            $.each(settings, function (index, elm) {

                user_settings[elm.name] = elm.value

            })
            

            var data = {
                system_settings: settings,
                user_settings:  user_settings
            }


            $http({
                method: 'POST',
                url: token_server_address + '/api/Settings/Save',
                data: data
            }).success(function (result) {
                return resolve(user_settings);
            });
        });
    }

    return o;
}
