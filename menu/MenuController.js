﻿

    angular
        .module('myApp.controllers')
        .controller('MenuController', ['$state', '$location', '$rootScope', '$log', 'hiEventService', 'authService', '$translate', Menu]);
    function Menu($state, $location, $rootScope, $log, hiEventService, authService, $translate) {

        var self = this;

        self.authentication = authService.authentication

        self.getClass = function (url) {

            if ('/#' + $location.path().substr(0, url.length - 2) == url || ($location.path() == '/home') && url == '/') {
                return "md-raised md-primary  md-hue-2"
            }
        }


        self.openMenu = function ($mdMenu, ev) {
            originatorEv = ev;
            $mdMenu.open(ev);
        };


        //$translate.use(authService.authentication.language);

        var t = function (key) {

            return $translate.instant(key)
        }
        
        if (self.authentication.isAuth) {
         
        }

        self.logout = function () {
            authService.logOut();            
            self.connection = null
            $rootScope.connection = null
            $location.path('/');
        }

        // when somebody is logging on
        authService.listen(function () {
            self.authentication = authService.authentication;
            self.get_menu(self.authentication.isAuth)

        })


        
        

        self.get_menu = function (isAuth) {

            self.menu_items = [{ url: '/', name: 'home' }]

            if (isAuth) {

                if (self.authentication.isAdmin) {
                    self.menu_items.push({ url: '/#/admin', name: 'Administration' })
                }
                
            } else {
                self.menu_items.push({ url: '/#/login', name: 'Log in' })
            }
        }

        self.get_menu(self.authentication.isAuth)

        

        // Highligting selected menu button
        self.getClass = function (url) {
            if ('/#' + $location.path().substr(0, url.length - 2) == url || ($location.path() == '/home') && url == '/') {
                return "md-raised md-primary  md-hue-2"
            } else {
                return "md-primary md-raised"
            }
        }
    }
   
