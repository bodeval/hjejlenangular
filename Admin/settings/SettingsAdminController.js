﻿

angular.module('myApp.controllers')
.controller('SettingsAdminController', ['settingsAdminService', '$scope', '$mdDialog', '$mdToast', '$rootScope', 'authService', '$location', 'mainService', SettingsAdminController]);

function SettingsAdminController(settingsAdminService, $scope, $mdDialog, $mdToast, $rootScope, authService, $location, mainService) {
    var self = this;

    self.settings = null

    //self.settings = cs.settings
    //mainService.settings = [{name: "max_number_of_persons", value: 98, type: 'number'},
    //        { name: 'auto_select_date', value: false, type: 'boolean' },
    //        { name: 'auto_open_departures', value: false, type: 'boolean'},
    //        { name: 'show_multible_unittypes', value:  false, type: 'boolean' },
    //        { name: 'show_months_ahead', value: 6, type: 'number' },
    //        { name: 'show_max_number_of_bikes', value: 66, type: 'number' },
    //        { name: 'cykel_statistik_type_id', value: 6 , type: 'boolean' }]
        

    
    settingsAdminService.get()
    .then(function (result) {

        self.settings = result.settings
    })
        
    self.add = function (ev) {

            $mdDialog.show({
                locals: { info: "" },
                controller: ['$scope', '$mdDialog', '$mdToast', EditAdminSettingsController],
                controllerAs: 'ctrl',
                templateUrl: '/content/angular/Admin/settings/_add_setting.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                preserveScope: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
            .then(function (setting) {

                $scope.status = 'Ok';
                self.settings.push(setting)
                
                settingsAdminService.save(self.settings)
                .then(function (result) {                
                    $scope.outerForm.$setPristine();
                    self.show_toast($scope.status)                    
                    mainService.settings = result
                    cs.settings = result                    
                })
                
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
    }

    
    self.save = function () {
        settingsAdminService.save(self.settings)
                .then(function (result) {
                    $scope.outerForm.$setPristine();
                    self.show_toast("Settings is saved")
                    mainService.settings = result                 
                    cs.settings = result                    
                })
    }

    self.show_toast = function (txt) {
        var toast = $mdToast.simple()
           .textContent(txt)
           .position('top right')
           .hideDelay(3000)
           .highlightAction(true)
           .parent("#toast_div");
        //.theme("custom-toast");                
        $mdToast.show(toast)
    }
}


function EditAdminSettingsController($scope, $mdDialog, $mdToast) {

    var self = this;

    self.title = "New Setting";
    self.setting_types = ['text', 'number', 'boolean']

    self.setting = {
        name: '',
        description: '',
        type: ''
    }

    self.save = function (setting) {
        $mdDialog.hide(setting);
    }

    $scope.hide = function () {
        $mdDialog.hide();
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.answer = function (answer) {
        $mdDialog.hide(answer);
    };
}
