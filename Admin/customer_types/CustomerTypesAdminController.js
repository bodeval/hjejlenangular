﻿

    angular.module('myApp.controllers')
    .controller('CustomerTypesAdminController', ['customerTypeAdminService', '$scope', '$mdDialog', '$mdToast', '$rootScope', 'authService', '$location', CustomerTypesAdminController]);

    function CustomerTypesAdminController(customerTypeAdminService, $scope, $mdDialog, $mdToast, $rootScope, authService, $location) {
        var self = this;

        self.sites = []

        self.customer_types = []

        customerTypeAdminService.getCustomerTypes()
        .then(function (result) {
            self.customer_types = result
        })


        customerTypeAdminService.getSites()
        .then(function (result) {
            self.sites = result
        })


        self.show_toast = function (txt) {
            var toast = $mdToast.simple()
               .textContent(txt)
               .position('top right')
               .hideDelay(3000)
               .highlightAction(true)
               .parent("#toast_div");
            //.theme("custom-toast");                

            $mdToast.show(toast)
        }

        self.add = function () {

            var new_type = {id: 0, "portal_id": 0, "company_id": 0, "company_customer_type": "", "portal_customer_type": "" }

            self.customer_types.unshift(new_type)
        }

        self.save = function (type) {
            customerTypeAdminService.save(type)
            .then(function () {
                self.show_toast("type er gemt !")
            })
        }

    }



