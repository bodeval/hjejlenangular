﻿(function(){
    
angular
         .module('myApp.controllers')
         .controller('MapController', [
            '$mdSidenav', '$mdBottomSheet', '$timeout', '$log', '$document', '$scope', '$mdToast', 'uiGmapGoogleMapApi', '$rootScope',  'uiGmapIsReady','mainService', 'mapService', 'tourService', '$window', 
            MapController
         ]);

function MapController($mdSidenav, $mdBottomSheet, $timeout, $log, $document, $scope, $mdToast, uiGmapGoogleMapApi, $rootScope, uiGmapIsReady, mainService, mapService, tourService, $window) {
        var self = this;
        self.map_loaded = false;
        $scope.markers = []
        
        $scope.showMap = false

        $scope.bounds_is_fit = false

    /*******************************************/
        // Refresh map on resize

        $scope.is_mobile = mainService.is_mobile

        $scope.$watch('is_mobile', function () {

            if ($scope.is_mobile == false) {
                if (mainService.harbors.length > 0) {
                    $rootScope.$broadcast("done");
                }
                 
            }
        });


        angular.element($window).bind('resize', function () {

            $scope.width = $window.innerWidth;
          
            if (window.innerWidth <= 800) {
                $scope.is_mobile = true
                
            } else {
                $scope.is_mobile = false

            }        
        });


     /****************************************************/  



        var map_styles = eval(JSON.parse(JSON.stringify(mainService.settings.map_styles)))
        $scope.options = {
            styles: map_styles
        };

        $scope.is_persons_selected = mainService.is_persons_selected

        $scope.$on('person_is_selected', function (event) {
            $scope.is_persons_selected = mainService.is_persons_selected

        });

        
        
        $scope.window = {
            marker: {},
            show: false,
            closeClick: function () {
                this.show = false;
            },
            options: {}, // define when map is ready
            title: '',
           
            show_continue_btn: false,
            show_start_btn: false
        }
        $scope.windowOptions = {
            visible: false
        };


        var center_coordinates = mainService.settings.google_coordinate_center.split(",")
        var center_lat = center_coordinates[0]
        var center_lng = center_coordinates[1]

        $scope.map = {
            center: {
                latitude: center_lat,
                longitude: center_lng,
            },
            zoom: mainService.settings.google_map_zoom,
            bounds: {},
            showTraficLayer: true,
            scrollwheel: true,
            control: {}
            
        };


        $scope.$watch($scope.map.control, function (elm) {

           


                $timeout(function () {

                    var a = $scope.map.control
                    a.getGMap().setCenter($scope.map.bounds.getCenter())
                    a.getGMap().fitBounds($scope.map.bounds)
                    $scope.bounds_is_fit = true

                }, 700)
            
        })
     
       
        
        $scope.map.polys = []
        $scope.maps = null
      
        $rootScope.$on("update_map", function () {
            createMarkers(mainService.map_harbors, $scope.maps)
        })

        
        $rootScope.$on("done", function () {
            // Google Maps Api loaded and ready
            


                uiGmapGoogleMapApi.then(function (maps) {

                    $scope.maps = maps
                    // $scope.showMap = false
                    self.map_loaded = true
            
                    createMarkers(mainService.map_harbors, maps)

                    $timeout(function () {
                        $scope.showMap = true;
                        //   $scope.$apply();
                    }, 100);

                    maps.event.trigger($scope.map, 'resize');
           


                });

        });

        function createMarkers(harbors, maps) {

            
            var bounds = new maps.LatLngBounds();
            $scope.map.bounds = bounds
            var state = mainService.booking_state            
            $scope.markers = []

            var end_harbor = tourService.end_harbor

            $.each(harbors, function (index, elm) {
                
                if (elm.lat != 0 && elm.lng != 0 ) {
           
                    var goldStar 

                    //var green_marker = '/content/Media/green_directions_boat.png'
                    //var green_marker = "/images/hjlogo_green.png"
                    var green_marker = "/images/stig_green.png"
                    var zIndex = 0;

                    if (elm.is_show_label) {
                        var goldStar = {
                            //path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
                            path: maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                            fillColor: '#0a7e77',
                            fillOpacity: 1.0,
                            scale: elm.is_show_label ? 0.0 : 0.0,
                            strokeColor: 'black',
                            strokeWeight: 0
                        };

                    } else {
                        //goldStar = '/content/Media/blue_directions_boat.png'
                        //goldStar = '/images/hjlogo.png'
                        goldStar = '/images/stig_blue.png'
                    }
                    
                    var labelContent = ''
                    if (state == -2) {
                      
                        if (elm.is_home) {
                            labelContent = '<span class="label_status"><i class="material-icons">home</i></span><span class="mcont">' + elm.name + '</span>'
                            if (!elm.is_show_label) {
                                goldStar = green_marker
                            }
                        } else {
                            if (elm.is_show_label) {
                                labelContent = '<span class="label_status"></span><span class="mcont">' + elm.name + '</span>'
                            } else {
                                labelContent = ''
                            }
                            
                        }

                    } else {
                                                    
                        if (elm.label_status == 0) {
                            labelContent = '<span class="label_status"><i class="material-icons">home</i></span><span class="mcont">' + elm.name + '</span>'
                            if (!elm.is_show_label) {
                                goldStar = green_marker
                                zIndex = 1000
                            }
                        } else if (elm.label_status == 333) {
                            labelContent = '<span class="label_status"><i class="material-icons">home</i><i class="material-icons">flag</i></span><span class="mcont">' + elm.name + '</span>'
                            if (!elm.is_show_label) {
                                goldStar = green_marker
                            }
                        } else if (elm.label_status > 0) {

                            var label_status_icon = ""
                            $.each(elm.tour_number, function (index, elm) {
                                label_status_icon += '<i class="tal">' + elm + ' </i> '
                            })
                           
                            labelContent = '<span class="label_status">' + label_status_icon + '</span><span class="mcont">' + elm.name + '</span>'

                            if (!elm.is_show_label) {
                                goldStar = green_marker
                                zIndex = 1000
                            }
                        
                        } else if (elm == end_harbor) {
                            var label_status_icon = ""
                            $.each(elm.tour_number, function (index, elm) {
                                label_status_icon += '<i class="tal">' + elm + ' </i> '
                            })
                            labelContent = '<span class="label_status">' + label_status_icon + '<i class="material-icons flag">flag</i></span><span class="mcont">' + elm.name + '</span>'

                            if (!elm.is_show_label) {
                                goldStar = green_marker
                                zIndex = 1000
                            }

                        } else {
                            labelContent = '<span class="label_status"></span><span class="mcont">' + elm.name + '</span>'
                        }
                    }

                    if (!elm.is_show_label) {
                        labelContent = ''
                    }
                    var marker = {
                      
                        id: index,
                        coords: {
                            latitude: elm.lat,
                            longitude: elm.lng
                        },
                        elm: elm,
                        icon: goldStar,
                        title: elm.name,
                        options: { labelClass: elm.css_class, labelAnchor: '40 35', labelContent: labelContent /* , labelStyle: newstyle */, title: elm.name, elm: elm,   zIndex: zIndex, icon: goldStar },
                        click: function (marker, eventName, model, arguments) {
                            $scope.window.show = true;                            
                            $scope.window.coords = model.coords;
                            $scope.window.title = marker.title;
                            
                            $scope.window.harbor = elm;

                            var selected_arrival_harbor = tourService.tours[0].selected_arrival_harbor
                            if (tourService.tours.length == 1 && selected_arrival_harbor == null) {
                                $scope.window.show_start_btn = true
                            } else {
                                $scope.window.show_start_btn = false
                            }

                            if (elm.css_class == 'marker_green' && tourService.home_harbor) {
                                $scope.window.show_continue_btn = true
                            } else {
                                $scope.window.show_continue_btn = false
                            }

                            // init start btn
                            $scope.window.start_here =  function () {

                                var t = tourService.get_first
                                t.select_departure_harbor(marker.elm)
                                

                                $scope.window.show = false;
                            }

                            // init continue btn
                            $scope.window.continue_here = function () {
                                
                                var current_tour = tourService.get_last_tour()
                                if (current_tour.selected_arrival_harbor) {
                                    tourService.add_tour()
                                    current_tour = tourService.get_last_tour()
                                }


                                current_tour.selected_arrival_harbor = marker.elm
                                current_tour.select_arrival_harbor()
                                $scope.window.show = false;
                            }
                        }
                    };

                    

                    var myLatLng = new maps.LatLng(elm.lat, elm.lng);
                   
                     bounds.extend(myLatLng);

                     
                    $scope.markers.push(marker)
                }


                if (bounds) {
                
              
                    $scope.map.bounds = bounds
                }
            })
        }
        
        $scope.markersEvents = {
            click: function (marker, eventName, model, arguments) {
               // debug.log(model)                
                $scope.window.model = model;               
                $scope.window.title = model.title;
                $scope.window.show = true;
            }
        }

        function drawpolys() {
            mainService.get_routes(mainService.ruter)
            .then(function (routes) {

                var polys = []
                var colors = ["red", "green", "blue", "orange", "pink"]
                $.each(routes, function (index, elm) {
                    var path = mainService.route_paths(elm.harbor_ids)

                    if (path.length == 3) { path.push(path[0]) }
                    var poly = {
                        "id": elm.Rute_id,
                        "path": path,
                        "stroke": {
                            "color": colors[index % 4],
                            "weight": 12
                        },
                        "editable": false,
                        "draggable": false,
                        "geodesic": true,
                        "visible": true
                    }
                    polys.push(poly)
                })
                $scope.map.polys = polys
            })
        }

        /* Showing Faaborg III realtime position */
        $scope.oferry = []
        function set_oferry_position(result) {
            
            var ret = {
                latitude: parseFloat(result.latitude),
                longitude: parseFloat(result.longitude),
                id: "test",
                title: "Øfærgen",
                idKey: "1"
            }
            ret['id'] = 1;

            $scope.oferry = [ret]
        }
    /*
        $timeout(function () {
            mapService.get_oferry_position()
            .then(function (result) {
                set_oferry_position(result)
            })
        }, 1000 * 420);

        mapService.get_oferry_position()
        .then(function (result) {
            set_oferry_position(result)
        })
        */
    }

})();

//https://www.marinetraffic.com/dk/ais/details/ships/shipid:155487/mmsi:219016906/imo:9628647/vessel:FAABORG_III/

//https://www.marinetraffic.com/dk/ais/details/ships/shipid:155641/mmsi:219017917/imo:9664524/vessel:STRYNOE/

//https://www.marinetraffic.com/dk/ais/details/ships/shipid:153535/mmsi:219000742/imo:9169794/vessel:HOJESTENE/
