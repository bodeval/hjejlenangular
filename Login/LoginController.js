﻿
(function () {
    angular.module('myApp.controllers')
    .controller('LoginController', ['LoginService', '$scope', '$mdDialog', '$mdToast', '$rootScope','authService','$location', LoginController]);

    function LoginController(LoginService, $scope, $mdDialog, $mdToast, $rootScope, authService, $location) {
        var self = this;
       
        self.downloading = false;
        self.authentication = authService.authentication
        self.login_result="";
        self.login_error="";
        self.username = ""
        self.password = ""
        $scope.message = ""

        self.loginData = {
            userName: "",
            password: ""
        };
     
     
         self.login = function (username, password) {
             self.downloading = true;
             self.loginData = {
                 userName: username,
                 password: password
             }

             // delay to visualize that the login i processing
          //   window.setTimeout(function () { 
                 authService.login(self.loginData).then(
                    function (response) {
                        self.downloading = false;

                        if (self.authentication.isAdmin) {
                            $location.path('/');
                        } else {
                            $location.path('/');
                        }
                        
                        self.downloading = true;
                    },
                    function (err) {
                        self.downloading = false;
                        self.login_error = err.error_description;
                     })                
          //   }, parseInt(Math.random(3000) * 1000))
        };



         self.register = function (username, password) {
             self.downloading = true;
             LoginService.register(username, password)
             .then(
                function (result) {
                    self.login_error = "";
                    self.downloading = false;
                    self.login_result = ['<h3>Oprettet</h3>', 'user: ' +username, 'password: ' + password];
                    


                },
                function (message) {
                    self.downloading = false;
                    if (message.ModelState) {
                        self.login_error = message.ModelState[""]
                    }
                }
             )
        }

        self.logout = function () {
            authService.logOut()
              
        }

    }

})();

