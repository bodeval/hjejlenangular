﻿angular.module('myApp.services')
    .factory('tourService', ['$http', '$q', 'mainService', '$filter', '$rootScope', '$document', '$timeout', '$translate', 'authService', TourService]);

function TourService($http, $q, mainService, $filter, $rootScope, $document, $timeout, $translate, authService) {

    
    var api_path = cs.api_url
    

    
    var o = {
        menu: [],
        list: [],
        current_date: new Date(),
        departureDates: [], /* todo*/
        tours: [],
        home_harbor: null,
        end_harbor: null,
        selected_unit_type: null,
        price_total: null,
        start_date: null,
        selected_ticket_type: null,
        is_ready_to_book: false,
        routing_state: false
        
        
    };

    var t = function (key) {
        return $translate.instant(key)
    }


   
    o.get_houres = function (item) {
        return moment(item.DatoTid).hour()
    }

    o.is_mob = function () {
        if (window.innerWidth <= 800) {
            return true;
        } else {

            return false;
        }
    }
    o.select_departure_harbor = function (item) {
        var first_tour = o.tours[0]
        first_tour.select_departure_harbor(item);
        
        // Get departures to check if departure is nonstop
        
        o.get_departures_by_date(new Date(first_tour.departure_date), item, item)
        .then(function (response) {
            
            var roundtrips = $.grep(response, function (elm) {
                return elm.starthavn_id == elm.sluthavn_id
            })


            var evening_with_hjejlen = []
            var evening_without_hjejlen = []
            var evening_from_ry = []
            var nonstop = []

          

            $.each(roundtrips, function (index, elm) {

                
                if (elm.ferry_name == "Hjejlen") {
                    evening_with_hjejlen.push(elm)
                } else if (elm.starthavn == "Ry") {
                    evening_from_ry.push(elm)
                } else if (moment(elm.DatoTid).hour() >= 18) {
                    evening_without_hjejlen.push(elm)
                } else {
                    nonstop.push(elm)
                }
            })

            

            $.each(mainService.ticket_types, function (index, elm) {

                switch (elm.type) {
                    case "nonstop":
            
                        elm.disabled = nonstop.length == 0

                        if (item.name == "Silkeborg") {
                            elm.show = true
                        } else {
                            elm.show = false
                        }

                        break;

                    case "evening":
                        if (item.name == "Silkeborg" || item.name == "Ry") {
                            elm.show = true
                        } else {
                            elm.show = false
                        }

                        elm.disabled = evening_without_hjejlen.length == 0 && evening_from_ry.length == 0 && evening_with_hjejlen == 0

                        elm.text = t(elm.type) + " " + item.name
                        if (evening_with_hjejlen.length > 0) {
                            elm.text += " med Hjejlen"
                        } else {
                            elm.text += " med turbåd"
                        }

                        break;
                    case "advanced":
                        
                        
                        // only silkebor himmelbjerget
                        if (o.home_harbor.arrival_harbor_ids.split(",").map(Number).indexOf(23) > -1 && (o.home_harbor.arrival_harbor_ids.split(",").map(Number).indexOf(32) > -1 || o.home_harbor.arrival_harbor_ids.split(",").map(Number).indexOf(31) > -1)) {
                            elm.show = !o.is_mob()                            
                        } else {
                            elm.show = false
                        }
                        break;
                    default:
                        elm.disabled = false
                        elm.show = true
                        break;
                }
            })

           

            $rootScope.$broadcast("departure_selected");
            $rootScope.$broadcast("update_map");
        })
    }


    o.check_ready_to_book = function () {
            var check = true
            $.each(o.tours, function (index, elm) {            
                if (!elm.selected_departure) {
                    check = false
                } 
            });

            o.is_ready_to_book = check
            $rootScope.$broadcast("is_ready_to_book")
    }


    o.settings = cs.settings

    o.get_departure_dates = function (str_start_date, str_end_date, departure_harbor_id, arrival_harbor_id) {
       return $q(function (resolve, reject) {
            $http({
                method: 'GET',
                url: api_path + '/api/diverse/GetPossibleDepartures?str_start_date=' + str_start_date + '&str_end_date=' + str_end_date + '&departure_harbor_id=' + departure_harbor_id + '&arrival_harbor_id=' + arrival_harbor_id
            }).success(function (result) {              
                o.departureDates = result;                
                return resolve(result);
            });
        });
    }

   

    o.get_second = function () {
        return o.tours[1];
    }

    o.get_first = function () {
        return o.tours[0];
    }
   
    o.get_last_tour = function () {
        return o.tours[o.tours.length - 1]
    }

    o.get_next_tour = function (tour) {
        return o.tours[o.tours.indexOf(tour) + 1]
    }

    o.get_previous_tour = function (tour) {
        return o.tours[o.tours.indexOf(tour) - 1]
    }

    o.add_tour = function () {
        var last_tour = o.get_last_tour()
      
        var tour = new o.tour()
        tour.index = o.tours.length 
        tour.departure_harbors.push(last_tour.selected_arrival_harbor)
        tour.selected_departure_harbor = last_tour.selected_arrival_harbor
        tour.departure_date = last_tour.departure_date     
        o.tours.push(tour)        
     
        return tour
    }

    o.delete_tour = function () {
        
        var t = o.tours.pop()

        
        if (t.selected_arrival_harbor) {
            if (t.selected_arrival_harbor.id == o.home_harbor.id) {

                t.selected_arrival_harbor.label_status = 0
            } else {
                t.selected_arrival_harbor.label_status = o.tours.length
            }
        }
        
        var last = o.get_last_tour()
        var prev = o.get_previous_tour(last)
        
        if (prev) {
            if (prev.selected_departure) {
                last.enable_select_departure_date = true
            }
        }
        last.enable_select_departure_harbor = true        
        o.update_map_harbors(last.selected_arrival_harbor,-2)
    }

    o.enable_add_tour = function () {
        var tour = o.get_last_tour()
        if (tour) {
            return tour.selected_departure_harbor != null && tour.selected_arrival_harbor != null
        }
    }

    o.clear_nexts = function (p) {
        var index = o.tours.indexOf(p) + 1
        for (var i = index; i < o.tours.length; i++) {
            o.tours[i].selected_departure = null
            o.tours[i].departure_date = null
        }        
    }

    o.update_map_harbors = function (harbor, state) {
        return $q(function (resolve, reject) {
            var r = harbor.arrival_harbor_ids.split(',').map(Number);
            switch (state) {

                case -2:                    
                    harbor.label_status = -2
                    harbor.tour_number.pop()
                    o.end_harbor = harbor
                    o.booking_state = 0
                    $.each(mainService.map_harbors, function (index, elm) {
                        
                        if (elm.id == harbor.id) {
                            elm.css_class = 'marker_blue'
                           // elm.label_status = -1
                            if (o.tours.length > 1) {
                                // departure_harbor  and arrival_harbor is equal so we are home
                                if (elm.id == o.home_harbor.id) {
                                    //elm.label_status = 333                                   
                                } 
                            }
                        } else if (r.indexOf(elm.id) > -1) {                           
                            elm.css_class = 'marker_green'
                        } else {                           
                            elm.css_class = 'marker_gray'
                        }
                    })
                    break;

                case -1:
                    o.booking_state = -1
                    $.each(mainService.map_harbors, function (index, elm) {
                        /* start harbor */                        
                        if (elm.id == harbor.id) {                        
                            elm.css_class = 'marker_blue'                          
                            if (o.tours.length > 1) {
                                // departure_harbor  and arrival_harbor is equal so we are home
                                if (elm.id == o.home_harbor.id) {
                                    elm.label_status = 333                                    
                                } else {
                                    elm.label_status = o.tours.length
                                    elm.tour_number.push(o.tours.length - 1)                                    
                                }
                            } else {
                                elm.label_status = 0
                            }
                            /* posible arrivals*/
                        } else if (r.indexOf(elm.id) > -1) {
                            elm.css_class = 'marker_green'

                        } else {
                            elm.css_class = 'marker_gray'
                        }
                    })                    
                    break;
                case 0:                    
                    o.booking_state = 0
                    $.each(mainService.map_harbors, function (index, elm) {
                        if (elm.id == harbor.id) {
                            elm.css_class = 'marker_blue'
                            elm.label_status = -1
                            if (o.tours.length > 1) {
                                // departure_harbor  and arrival_harbor is equal so we are home
                                if (elm.id == o.home_harbor.id) {
                                    elm.label_status = 333
                                }
                            }
                        } else if (r.indexOf(elm.id) > -1) {
                            elm.css_class = 'marker_green'
                        } else {
                            elm.css_class = 'marker_gray'
                        }
                    })
                    break;
            }
            return resolve($rootScope.$broadcast("done"));
        })
    }


   
 /******************************************************************************************************************************************************************
 ****************************************************** T O U R ***************************************************************************************************
 ******************************************************************************************************************************************************************/
    o.tour = function () {

        var p = {
            selected_departure_harbor: null,
            selected_arrival_harbor: null,
            departures: [],
            departure_date: null,
            departure_harbors: [],
            arrival_harbors: [],
            loading_departures: false,
            filtered_departures: [],
            selected_departures: null,
            minDate: null,
            enable_select_departure_harbor: true,
            enable_select_departure_date: false,
            selected_departure: null,
            date_change: this.date_change,
            start_date_time: new Date(),
            price: null,
            index: 0,
            prioritized_departure_harbors: [],
            prioritized_arrival_harbors: [],
            selected_ticket_type: null,
            arrival_time: null
            
        }

        // check if every crossing is available
        p.check_path = function (path) {            
            var result = false            
            $.each(path, function (index, item) {
                if(item.Udsolgt == true || item.Locked == true || mainService.number_of_persons + item.SumPersons > item.MaxPers || (o.selected_unit_type.number_of_items ? item.SumNumberBikes + o.selected_unit_type.number_of_items > item.max_number_of_bikes : false)) {                    
                    result = true
                    return false;                    
                }
            })
            return result;
        }

        p.check_is_disabled = function (item) {

            var result = (!p.in_time(item)) || item.Udsolgt == true || item.Locked == true || mainService.number_of_persons + item.SumPersons > (authService.authentication.isAuth ? item.MaxPers : (item.MaxPers * item.PersonBookSelvPct / 100))  || (o.selected_unit_type.number_of_items ? item.SumNumberBikes + o.selected_unit_type.number_of_items > item.max_number_of_bikes : false) || p.check_path(item.path)
            item.is_disabled = result
            return result
        }

        p.get_prioritized_harbors = function (harbors) {
            var prioritized_harbors = []
            
            $.each(o.settings.primary_harbors.trim().split(/\s*,\s*/), function (index, elm) {
                var h = $.grep(harbors, function (elm2) {
                    return elm2.initialer.toLowerCase().trim() == elm.trim().toLowerCase()
                })
                if (h.length > 0) {
                    prioritized_harbors.push(h[0])
                }
            })
            return prioritized_harbors
        }


        p.get_tour_note = function () {
            var result = "";
            if (p.index > 0) {
                if (p.departure_date) {
                    
                    var previous_tour = o.tours[p.index - 1]

                    var diff = moment(p.departure_date).diff(moment(previous_tour.departure_date), "days")
                    
                    if (diff > 0) {
                        result = "<span>Bemærk:</span> " + diff + " " + (diff < 2 ?  " dag" : " dage ") + " til næste overfart"
                    }                    
                }
            }
            return result;
        }

        p.is_last = function () {
            return p == o.get_last_tour()
        }

        p.get_price = function () {
            return $q(function (resolve, reject) {


                try {


                    var departure_id = p.selected_departure.Afgangs_id
                    var unit_type = o.selected_unit_type
                    var unit_type_id = unit_type.Enhedstype_id
                    var number_of_items = 1

                    if (unit_type.KravStk) {
                        number_of_items = unit_type.number_of_items
                    }
                    var person_types = []
                    $.each(unit_type.person_types, function (index, elm) {
                        if (elm.Amount > 0) {
                            person_types.push({ Id: elm.id, Amount: elm.Amount })
                        }
                    })


                    var tour_ids = []
                    $.each(o.tours, function (index, elm) {
                        var departure_ids = []
                        departure_ids.push(elm.selected_departure.Afgangs_id)

                        $.each(elm.selected_departure.path, function (index2, elm2) {
                            if (elm2) {
                                departure_ids.push(elm2.Afgangs_id)
                            } else {
                           
                            }
                        
                        })

                        tour_ids.push(departure_ids)
                    })

                    var price_request = {
                        "DepartureIds": tour_ids,
                        "PersonTypes": person_types,
                        "UnitType": {
                            "Id": unit_type_id,
                            "NumberOfItems": number_of_items
                        }
                    }

                    $http({
                        method: 'POST',
                        url:  api_path +  '/api/Price/GetPrice',
                        data: price_request
                    })
                    .success(function (result) {
                        if (result == null) {
                        
                            // o.price_total = Math.round(Math.random(10000) * 1000, 2)
                            //$rootScope.$broadcast("priceUpdated");
                            return resolve();
                        } else {
                            o.price_total = result
                            $rootScope.$broadcast("priceUpdated");
                            return resolve(result);
                        }
           
                    })

                } 
                catch (e) {

                    console.log("fejl i prishentning")
                    console.log(e)
                    reject(e)
                }

             });

        }

        p.enable_select_date = function () {
            var length = o.tours.length
            switch (length) {
                case 0:
                    return false;
                    break;
                case 1:                    
                    return this.selected_departure_harbor && this.selected_arrival_harbor
                    break;
                default:
                    var index = o.tours.indexOf(this)
                    if (index == 0) {
                        return this.selected_departure_harbor && this.selected_arrival_harbor
                    } else {
                        var prev_tour = o.tours[index]
                        return prev_tour.departure_date != null
                    }
            }
        }

        p.minDate = new Date(
                o.current_date.getFullYear(),
                o.current_date.getMonth(),
                o.current_date.getDate());


        p.tour_date_clicked = function (date) {
            p.departure_date = date
        }

        p.get_departures = function () {
            return $q(function (resolve, reject) {
                
                o.get_departures_by_date(new Date(p.departure_date), p.selected_departure_harbor, p.selected_arrival_harbor)
              .then(function (result) {
                
                  p.loading_departures = false
                  p.selected_departures = result
                  return resolve()
              })
            })
        }

        p.date_change = function (date) {
            
            p.departure_date = date
            p.selected_departure = null
            p.loading_departures = true
            o.get_departures_by_date(date, p.selected_departure_harbor, p.selected_arrival_harbor)
            .then(function (result) {
                

                p.loading_departures = false
                p.selected_departures = result

                if (p.selected_arrival_harbor) {

                    var str_start_date = moment(date).format("DD-MM-YYYY")

                    $http.get(api_path + '/api/UnitTypes/GetHarborsByDate?portalId=' + mainService.portalId + '&start_date=' + str_start_date)
                    .success(function (result) {
                        // finding new values

                        $.each(result, function (index, elm) {
                            // map_harbors opdateres med ny driftplan
                            var current_map_harbor = $.grep(mainService.map_harbors, function (elm2) {
                                return elm2.id == elm.havne_id
                            })[0]
                            
                            current_map_harbor.arrival_harbor_ids = elm.arrival_harbor_ids
                            if (elm.secondary_arrival_harbor_ids) {
                                current_map_harbor.arrival_harbor_ids = current_map_harbor.arrival_harbor_ids + ", " + elm.secondary_arrival_harbor_ids
                            }
                        })

                        
                        o.update_map_harbors(p.selected_arrival_harbor, 0)
                        $rootScope.$broadcast("update_map");
                        
                    });
                }
            })

            o.clear_nexts(p)
        }



        p.in_time = function (item) {
            var item_date = json2date(item.DatoTid)
            var lukketid = item.Akt_lukketid

            // hvis det er telefærge f.x. hjorø skal aktlukketid bruges
            if (item.Akt_lukketid_IfBookings) {
                if (item.SumBookings > 0) {
                    lukketid = item.Akt_lukketid_IfBookings
                }
            }
            var item_lukketid = $filter('minutesToDateTime')(item_date, -lukketid)            
            return item_lukketid >= p.start_date_time
        }

        p.get_arrival_time = function (item) {
            var result
            if (item != 'undefined') {
                
                result = json2date(item.DatoTid).addMinutes(item.sejltid)
            } else {
                result = json2date(p.DatoTid).addMinutes(p.sejltid)
            }
            
            return result
        }

        p.in_time_status = function (item) {
            //var item_date = new Date(moment(item.DatoTid).toLocaleString())
            var item_date = json2date(item.DatoTid)
            var lukketid = item.Akt_lukketid

            // hvis det er telefærge f.x. hjorø skal aktlukketid bruges
            if (item.Akt_lukketid_IfBookings) {
                if (item.SumBookings > 0) {
                    lukketid = item.Akt_lukketid_IfBookings
                }
            }

            var item_lukketid = $filter('minutesToDateTime')(item_date, -lukketid)

            if (item_date >= new Date()) {
                if (item_lukketid < new Date()) {
                    return { status: 0, message: 'bookingtid overskredet', Akt_lukketid: lukketid }
                } else {
                    return { status: 1, message: 'Ok', Akt_lukketid: lukketid }
                }
            } else {
                return { status: -1, message: 'udløbet', Akt_lukketid: lukketid }
            }
        }

        p.select_departure = function (selected_departure) {
            var dep_date = json2date(selected_departure.DatoTid)

            p.selected_departure = selected_departure


            o.check_ready_to_book()
            
        }

        p.select_departure_harbor = function (harbor) {
            
            // If first tour setting home
            if (o.tours.indexOf(p) == 0) {
                if (o.home_harbor) {                    
                    o.home_harbor.is_home = false
                   o.home_harbor.label_status = -2
                    o.home_harbor.state = -2                    
                }
                harbor.is_home = true
                o.home_harbor = harbor
                o.end_harbor = null
            }
          

           // if (mainService.home_harbor == null) {
                mainService.home_harbor = harbor
            //}

            p.selected_departure_harbor = harbor
            p.selected_arrival_harbor = null
            p.arrival_harbors = mainService.get_arrival_harbors(harbor)         

            
            p.prioritized_arrival_harbors = p.get_prioritized_harbors(p.arrival_harbors)

            /***************************************************************************************************************************/

            o.update_map_harbors(harbor, -1 /* state: departure selected */)
            /// todo: get imposible dates by departure- and arrival-harbor
            //o.departureDates.push(new Date(2017, 3, 27))
        }

        p.select_arrival_harbor = function (item) {            
            return $q(function (resolve, reject) {
                p.selected_arrival_harbor = item
                mainService.booking_state = 0
                o.end_harbor = p.selected_arrival_harbor
                o.update_map_harbors(p.selected_arrival_harbor, 0)
                p.get_departures().then(function () {                    
                    return resolve()               
                })
            });          
        }

        p.notPosibleDeparturesPredicate = function (date) {
            return $filter('filterDepartureDates')(date, o.departureDates);
        };
        return p;
    }

    o.get_departures_by_date = function (date, departure_harbor, arrival_harbor) {       
            var p = {}
            p.departures = []
            p.selected_departure_harbor = departure_harbor
            p.selected_arrival_harbor = arrival_harbor
            p.loading_departures = true
            
        //var departure_date = date.toISOString()
            var departure_date = moment(date).format('YYYY-MM-DDTHH:mm:ss')
            
            p.calculate_routes = function () {                
                var result = [];

                

                if (p.selected_arrival_harbor && p.selected_departure_harbor) {
                    for (var i = 0; i < p.departures.length; i++) {

                        if (p.departures[i].starthavn_id == p.selected_departure_harbor.id) {

                            p.departures[i].path = []
                            p.departures[i].isGood = true
                            p.departures[i].sejltid = p.departures[i].Overfartstid

                            p.departures[i].start_date_time = json2date(p.departures[i].DatoTid)
                            
                            if (p.departures[i].sluthavn_id != p.selected_arrival_harbor.id) {
                                if (p.departures[i].naeste != 0) {
                                    // Calling recursiv function traversing rest of the departures
                                    p.get_next_departure(i, i)
                                }
                            }

                            if (p.departures[i].naeste != 0 || p.departures[i].sluthavn_id == p.selected_arrival_harbor.id) {
                                result.push(p.departures[i])
                            }
                        }
                    }                    
                }

                $.each(result, function (index, elm) {
                    if (elm.starthavn_id == p.selected_departure_harbor.id) {
                        if (elm.sluthavn_id == p.selected_arrival_harbor.id) { 
                            elm.isGood = true                        
                        } else {
                            if (elm.path) {
                                if (elm.path[elm.path.length - 1].sluthavn_id == p.selected_arrival_harbor.id) {
                                    elm.isGood = true
                                } else {
                                    elm.isGood = false
                                }
                            } else {
                                elm.isGood = false
                            }
                        }
                    }
                })


                return $.grep(result, function (elm) {
                    return elm.isGood == true
                })
            }

            p.get_next_departure = function (index, i) {
                for (var j = i + 1; j < p.departures.length; j++) {
                    if (p.departures[i].naeste == p.departures[j].Afgangs_id) {
                       // if ((p.departures[j].sluthavn_id == p.selected_departure_harbor.id && (p.departures[index].path.length == 0)) || p.departures[j].naeste == 0 || p.departures[i].Rute_Id != p.departures[j].Rute_Id || p.departures[j].starthavn_id == p.selected_departure_harbor.id) {

                        if ((p.departures[j].sluthavn_id == p.selected_departure_harbor.id && (p.departures[index].path.length == 0)) || p.departures[i].Rute_Id != p.departures[j].Rute_Id || p.departures[j].starthavn_id == p.selected_departure_harbor.id ) {
                        // p.departures[j].naeste == 0 ||
                        // abc sidste condition er ikke gennemtestet
                        //
                        // ok
                      //  if ((p.departures[j].sluthavn_id == p.selected_departure_harbor.id && (p.departures[index].path.length == 0)) || p.departures[i].Rute_Id != p.departures[j].Rute_Id || p.departures[j].starthavn_id == p.selected_departure_harbor.id || p.departures[j].Rute_Id != p.departures[0].Rute_Id) {
                            p.departures[index].path = null
                            p.departures[index].isGood = false;
                            
                        } else {
                            
                            p.departures[index].path.push(p.departures[j])
                            p.departures[index].sejltid += p.departures[j].Overfartstid

                            if (p.departures[j].sluthavn_id != p.selected_arrival_harbor.id) {
                                p.get_next_departure(index, j)
                            }
                        }
                        break;
                    }
                }
            }

            return $q(function (resolve, reject) {     
                
                $http.get(api_path + '/api/diverse/GetDeparturesByDate?departure_date=' + departure_date + '&portal_id=' +  mainService.portalId + '&ruter=' + mainService.ruter)
                    .success(function (result) {                        
                        
                        p.departures = result
                        p.selected_departures = p.calculate_routes()
                        return resolve(p.selected_departures)                    
                    });
            });        
    }
    return o;
};