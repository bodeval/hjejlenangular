﻿

    angular.module('myApp.controllers')
    .controller('PortalMappingAdminController', ['portalMappingAdminService', '$scope', '$mdDialog', '$mdToast', '$rootScope', 'authService', '$location', PortalMappingAdminController]);

    function PortalMappingAdminController(portalMappingAdminService, $scope, $mdDialog, $mdToast, $rootScope, authService, $location) {
        var self = this;

        
        self.portal_sites = {}
        self.portal_person_types = {}
        self.portal_unit_types = {}



        portalMappingAdminService.get_portal_sites()
        .then(function (result) {        
            
            self.portal_person_types = result.portal_person_types
            self.portal_unit_types = result.portal_unit_types

            $.each(result.sites, function (index, elm) {
                elm.portal_person_types = angular.copy(self.portal_person_types)


                $.each(elm.portal_person_types, function (index2, elm2) {
                    $.each(elm.person_type_relations, function (index,elm3) {
                        if (elm3.PortalPersonTypeId == elm2.id) {
                            elm2.site_person_type_id = elm3.SitePersonTypeId
                        }
                    })                  
                })


                elm.portal_unit_types = angular.copy(self.portal_unit_types)

                $.each(elm.portal_unit_types, function (index2, elm2) {
                    $.each(elm.unit_type_relations, function (index, elm3) {
                        if (elm3.PortalUnitTypeId == elm2.Enhedstype_id) {
                            elm2.site_unit_type_id = elm3.SiteUnittypeId
                        }
                    })
                })
            })


            self.portal_sites = result.sites

            
        })


        self.save = function (site) {

            
            portalMappingAdminService.save(site)
            .then(function () {
                self.show_toast("Mapping er gemt")
            })
            
        }

        self.show_toast = function (txt) {
            var toast = $mdToast.simple()
               .textContent(txt)
               .position('top right')
               .hideDelay(3000)
               .highlightAction(true)
               .parent("#toast_div");
            //.theme("custom-toast");                

            $mdToast.show(toast)
        }

    }



