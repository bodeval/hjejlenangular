﻿(function(){
  'use strict';

  
  angular
              .module('myApp', ['myApp.services', 'ui.router', 'ngMaterial', 'myApp.controllers', 'uiGmapgoogle-maps', 'LocalStorageModule', 'pascalprecht.translate', 'ui.sortable', 'ngAnimate', 'luegg.directives'])
              .run(['$rootScope', '$window', function ($rootScope, $window) {
                  $rootScope.SiteID = "";

                  var lang = $window.navigator.language || $window.navigator.userLanguage;
                  var browser_lang = lang.substring(0, 2).toLowerCase()
                  
                  if (browser_lang != "") {
                      if ("da, de, en".split(",").indexOf(browser_lang) >= 0) {                          
                          $rootScope.language = browser_lang //| "da";
                      } else {                          
                          $rootScope.language = "en"
                      }
                      
                  } else {
                      $rootScope.language = "da";
                  }
                  
                  moment.locales($rootScope.language)
                  
              }])
            .config(['$mdThemingProvider', function ($mdThemingProvider) {


                $mdThemingProvider.definePalette('black', {
                    '50': '000000',
                    '100': '000000',
                    '200': '000000',
                    '300': '000000',
                    '400': '000000',
                    '500': '000000',
                    '600': '000000',
                    '700': '000000',
                    '800': '000000',
                    '900': '000000',
                    'A100': '000000',
                    'A200': '000000',
                    'A400': '000000',
                    'A700': '000000',
                    'contrastDefaultColor': 'light'
                });
                $mdThemingProvider.definePalette('white', {
                    '50': 'ffffff',
                    '100': 'ffffff',
                    '200': 'ffffff',
                    '300': 'ffffff',
                    '400': 'ffffff',
                    '500': 'ffffff',
                    '600': 'ffffff',
                    '700': 'ffffff',
                    '800': 'ffffff',
                    '900': 'ffffff',
                    'A100': 'ffffff',
                    'A200': 'ffffff',
                    'A400': 'ffffff',
                    'A700': 'ffffff',
                    'contrastDefaultColor': 'dark'
                });


                var hjejlen_blue = $mdThemingProvider.extendPalette('blue', {
                    '500': '#0A2996'
                });
                $mdThemingProvider.definePalette('hjejlen_blue', hjejlen_blue);

                var hjejlen_green = $mdThemingProvider.extendPalette('green', {
                    '500': '#0A7e77'
                });
                $mdThemingProvider.definePalette('hjejlen_green', hjejlen_green);

                var hjejlen_light_green = $mdThemingProvider.extendPalette('green', {
                    '500': '#cee5e4',
                    'contrastDefaultColor': 'dark'
                });

                // Register the new color palette map with the name <code>neonRed</code>
                $mdThemingProvider.definePalette('hjejlen_light_green', hjejlen_light_green);
                $mdThemingProvider.theme('default')
                    .primaryPalette('blue')
                    .accentPalette('red');

                $mdThemingProvider.theme('custom-toast')
                .primaryPalette('purple') // specify primary color, all
                .accentPalette('red');   // other color intentions will be inherited                  

                $mdThemingProvider.theme('hjejlen')
                .primaryPalette('hjejlen_blue') // specify primary color, all                
                .accentPalette('white')  // other color intentions will be inherited   
                .warnPalette('hjejlen_light_green');

                $mdThemingProvider.theme('custom')
              .primaryPalette('purple') // specify primary color, all
              .accentPalette('green', {
                  'default': '700' // use shade 200 for default, and keep all other shades the same
              });             
      }])

            .config([
                '$stateProvider',     
                '$urlRouterProvider',  
                '$locationProvider',
 
                function ($stateProvider, $urlRouterProvider, $locationProvider) {

                    $locationProvider.reloadOnSearch =  false
                    $locationProvider.html5Mode({
                        enabled: false,
                        requireBase: false
                    });

                    $stateProvider.reloadOnSearch = false

                    $stateProvider
                    .state('home', {
                        url: '/home/:startdate',
                        templateUrl: '/Content/angular/home/_home.html',
                        controller: 'HomeController as ctrl',
                        reloadOnSearch: false,
                        
                        })
                        .state('rundtur', {
                            url: '/rundtur',
                            templateUrl: '/Content/angular/home/_advanced.html',
                            controller: 'HomeController as ctrl',
                            reloadOnSearch: false
                        })
                      .state('map', {
                          url: '/map',
                          templateUrl: '/Content/angular/home/_map.html',
                          controller: 'MapController'
                      })
                     .state('admin', {
                         url: '/admin',
                         templateUrl: '/Content/angular/admin/_admin.html',
                         controller: 'AdminController as ctrl'
                     })
                    .state('login', {
                        url: '/login',
                        templateUrl: '/Content/angular/login/_login.html',
                        controller: 'LoginController as ctrl'
                    })
                    .state('register', {
                        url: '/register',
                        templateUrl: '/Content/angular/admin/_register.html',
                        controller: 'LoginController as ctrl'
                    }).state('user_admin', {
                        url: '/admin/user',
                        templateUrl: '/Content/angular/admin/users/_users_admin.html',
                        controller: 'UserAdminController as ctrl'
                    }).state('ruter_admin', {
                        url: '/admin/ruter',
                        templateUrl: '/Content/angular/admin/ruter/_ruter_admin.html',
                        controller: 'RuterAdminController as ctrl'
                    }).state('customer_types_admin', {
                        url: '/admin/customer_types',
                        templateUrl: '/Content/angular/admin/customer_types/_customer_types_admin.html',
                        controller: 'CustomerTypesAdminController as ctrl'
                    }).state('portal_unit_type_mapping_admin', {
                        url: '/admin/portal_mapping',
                        templateUrl: '/Content/angular/admin/portal_mapping/_portal_unit_type_mapping_admin.html',
                        controller: 'PortalMappingAdminController as ctrl'
                    }).state('portal_settings_admin', {
                        url: '/admin/settings',
                        templateUrl: '/Content/angular/admin/settings/_settings_admin.html',
                        controller: 'SettingsAdminController as ctrl'
                    });

                    $urlRouterProvider.otherwise('home/');
                }
            ])
    .config(['uiGmapGoogleMapApiProvider', function (uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({            
            key: 'AIzaSyDYtYj3vwd-7FdPa3wSzRwuhHJBE61ti2o',
            libraries: 'visualization',
            language: 'da'
        });
    }])
    .config(['localStorageServiceProvider', function (localStorageServiceProvider) {
        localStorageServiceProvider
          .setPrefix('hjejlen');
    }])


  .config([    
    '$mdThemingProvider', function ($mdThemingProvider) {

        $mdThemingProvider.theme('menu')
           .primaryPalette('grey', {
               'default': '50', // by default use shade 400 from the pink palette for primary intentions
               'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
               'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
               'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
           })
           .accentPalette('blue', {
               'default': '100', // by default use shade 400 from the pink palette for primary intentions
               'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
               'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
               'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
           }).backgroundPalette('blue', {
               'default': '100', // by default use shade 400 from the pink palette for primary intentions
               'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
               'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
               'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
           });

        $mdThemingProvider.theme('default')
            .primaryPalette('green', {
                'default': '400', // by default use shade 400 from the pink palette for primary intentions
                'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
                'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
                'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
            })
            .accentPalette('blue', {
                'default': '400', // by default use shade 400 from the pink palette for primary intentions
                'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
                'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
                'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
            })
             .warnPalette('red', {
                 'default': '400', // by default use shade 400 from the pink palette for primary intentions
                 'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
                 'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
                 'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
             })
    }
  ])

.run(['$rootScope', '$location', '$state', '$stateParams', '$urlRouter', function ($rootScope, $location, $state, $stateParams, $urlRouter) {

    var history = [];

    $rootScope.$on('$routeChangeSuccess', function() {
        
        history.push($location.$$path);
    });

    $rootScope.back = function () {
        var prevUrl = history.length > 1 ? history.splice(-2)[0] : "/";
        $location.path(prevUrl);
    };
    
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    $rootScope.api_path = cs.api_url



    /* if rutepath */
    $rootScope.$on('$locationChangeSuccess', function () {
        
        //$rootScope.sort = $location.search().sort;
        //$rootScope.order = $location.search().order;
        //$rootScope.offset = $location.search().offset;
    });
   

}])	

.filter('trim', function () {
    return function(value) {
        if(!angular.isString(value)) {
            return value;
        }  
        return value.replace(/^\s+|\s+$/g, ''); // you could use .trim, but it's not going to work in IE<9
    };
})
.filter('filterDepartureDates', function () {
    return function (date, departureDates) {                
        return $.grep(departureDates, function (elm) {           
            return moment(date).format("DD-MM-YYYY") == moment(elm).format("DD-MM-YYYY")
        }).length > 0        
    };
})
    .filter('minutesToDateTime', [function () {
        /*
         * in view {{ item.DatoTid | minutesToDateTime : item.Akt_lukketid  | date:' HH:mm'}}
         */
        return function (date, minutes) {
            return new Date(date).addMinutes(minutes);
        };
    }])
.config(['$mdDateLocaleProvider', function ($mdDateLocaleProvider) {

    //moment.locales($rootScope.language)
    
    
    var localeDate = moment.localeData("da");

    $.datepicker.setDefaults($.datepicker.regional['da']);

    //$mdDateLocaleProvider.shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'];

    $mdDateLocaleProvider.shortMonths = localeDate._monthsShort;

    $mdDateLocaleProvider.firstDayOfWeek = 1;
    //$mdDateLocaleProvider.shortDays = ['S', 'M', 'Ti', 'O', 'To', 'F', 'L'];

    $mdDateLocaleProvider.shortDays = localeDate._weekdaysMin;

    $mdDateLocaleProvider.formatDate = function (date) {
        return date ? moment(date).format('DD-MM-YYYY') : null;
        //return moment(date).format('DD-MM-YYYY');
    };

    $mdDateLocaleProvider.parseDate = function (dateString) {
        var m = moment(dateString, 'DD-MM-YYYY', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };


}])
.directive('selectOnClick', ['$window', function ($window) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('click', function () {
                if (!$window.getSelection().toString()) {
                    // Required for mobile Safari
                    this.setSelectionRange(0, this.value.length)
                }
            });
        }
    };
}])
.directive('datepicker', ['$rootScope', function ($rootScope) {
  
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };          

            var options = {
                dateFormat: "dd-mm-yy",
                onSelect: function (dateText) {
                    updateModel(dateText);
                },             
                numberOfMonths: 2,
                showButtonPanel: false,
                minDate: '0',
                inline: true,
                
                xmaxDate: '+5m',
                maxDate: json2date(cs.settings.maxDate),
            };
            elem.datepicker(options);
            
        }
    }}])


.directive('formLocator', function () {
    return {
        link: function (scope) {
            scope.$emit('formLocator');
        }
    }
})
.filter('rawHtml', ['$sce', function($sce){
    return function(val) {
        return $sce.trustAsHtml(val);
    };
 }])
 .directive('capitalize', function() {
          return {
              require: 'ngModel',
              link: function(scope, element, attrs, modelCtrl) {
                  var capitalize = function(inputValue) {
                      if (inputValue == undefined) inputValue = '';
                      var capitalized = inputValue.toUpperCase();
                      if (capitalized !== inputValue) {
                          modelCtrl.$setViewValue(capitalized);
                          modelCtrl.$render();
                      }
                      return capitalized;
                  }
                  modelCtrl.$parsers.push(capitalize);
                  capitalize(scope[attrs.ngModel]); // capitalize initial value
              }
          };
      })
       .directive('uncapitalize', function () {
           return {
               require: 'ngModel',
               link: function (scope, element, attrs, modelCtrl) {
                   var uncapitalize = function (inputValue) {
                       if (inputValue == undefined) inputValue = '';
                       var uncapitalize = inputValue.toLowerCase();
                       if (uncapitalize !== inputValue) {
                           modelCtrl.$setViewValue(uncapitalize);
                           modelCtrl.$render();
                       }
                       return uncapitalize;
                   }
                   modelCtrl.$parsers.push(uncapitalize);
                   uncapitalize(scope[attrs.ngModel]); // uncapitalize initial value
               }
           };
       })
     .run(['$http', function ($http) {
         // $http.defaults.headers.common.Authorization = 'Bearer -BljV78OXVcuN1O_IPQlu2cfdycFEqiJwazP6zVzIrxtWGCazXvWgass5XZghEF5ZluaLUsXr_pn_uAEvHpD_Z3_G2rW7QCiEzKDjnxtFS8OB9LdOlKm6p_Y_IfyQuES-nmu64D4DuyKb2aOgUEy2K6jThWo9TsOQd7R4xXK7fEIJues8LcdHESKXnW7M8TPbkluZZ6xEfy6ouFd9gC_LQ_6zZeYaFEnxVcDPGZiBSblRtTDoCUiW9vqHSnqzKdLX0jVe2ec0ys-arxwpw211uTXmp0HC3ibMnLV-vbtVFY0m6r_bfipcnOJQnPten8LJZxmndn3ooT-xrLbXhFNMfVQXOpgxNEG_M0MrhfpyO4875B5cSPvhZLBVRbVUrqE0J9uCOthQ1rOiR00gBfVarrA6DnOSv8Jggp3jgcLlHkuw85GBOwssga9zzHXCYl4y6gHHQkFiQY_GxDIngZZ5_jYcwSwpd-48DgSef3tuaE';
     }])
    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptorService');


        // prevent caching
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }

        // Answer edited to include suggestions from comments
        // because previous version of code introduced browser-related errors

        //disable IE ajax request caching
        //$httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
        //// extra
        //$httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
        //$httpProvider.defaults.headers.get['Pragma'] = 'no-cache';




    }])
    .run(['authService', function (authService) {
        authService.fillAuthData();
    }])
    .service("hiEventService",['$rootScope', function ($rootScope) {
        this.broadcast = function () { $rootScope.$broadcast("hi") }
        this.listen = function (callback) { $rootScope.$on("hi", callback) }
    }])
 

})();


var cs = {}

cs.checkUrlHttp = function(str)
{
    var url = $.trim(str);
    if (url.indexOf("http://") == 0 || url.indexOf("https://") == 0) {
        return url
    } else {
        if (url != "") {
            return "http://" + url
        } else {
            return ""
        }        
    }
}

angular.module('myApp.services',[])
angular.module('myApp.controllers', ['myApp.services']);